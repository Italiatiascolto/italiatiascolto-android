package it.italiatiascolto.app.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import it.italiatiascolto.app.R
import java.util.*

class SplashScreenActivity : BaseActivity() {
    val REQUEST_CODE_SIGN_IN = 100
    private var mCurrentUser: FirebaseUser? = null
    private lateinit var mFirebaseRemoteConfig: FirebaseRemoteConfig

    override val contentView = R.layout.activity_splashscreen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadRemoteConfiguration()
    }

    override fun onResume() {
        super.onResume()
        checkAuthentication()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SIGN_IN) {
            val response: IdpResponse? = IdpResponse.fromResultIntent(data)!!
            if (resultCode == RESULT_OK) {
                MainActivity.start(this)
                finish()
            } else {
                if (response == null) {
                    return
                }
                if (response.error?.errorCode == ErrorCodes.NO_NETWORK) {
                    return
                }
            }
        }
    }

    private fun loadRemoteConfiguration() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        mFirebaseRemoteConfig.fetchAndActivate()
            .addOnSuccessListener {

            }
            .addOnCanceledListener {

            }
            .addOnFailureListener {

            }
    }

    private fun checkAuthentication() {
        mCurrentUser = FirebaseAuth.getInstance().currentUser
        if (mCurrentUser != null) {

        } else {
            startAuthenticationFlow()
        }
    }

    private fun startAuthenticationFlow() {
        val authIntent = AuthUI.getInstance()
            .createSignInIntentBuilder()
            .setAvailableProviders(Arrays.asList(
                AuthUI.IdpConfig.GoogleBuilder().build(),
                AuthUI.IdpConfig.FacebookBuilder().build(),
                AuthUI.IdpConfig.EmailBuilder().build()
            )).build()
        startActivityForResult(
            authIntent,
            REQUEST_CODE_SIGN_IN
        )
    }

}
package it.italiatiascolto.app.ui.activities

import android.app.Activity
import android.os.Bundle

abstract class BaseActivity: Activity() {

    open val contentView = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentView)
    }
}
package it.italiatiascolto.app.ui.fragments

import androidx.databinding.ViewDataBinding

abstract class NavigationFragment<T: ViewDataBinding>: BaseFragment<T>() {

    override fun loadContent() {

    }
}
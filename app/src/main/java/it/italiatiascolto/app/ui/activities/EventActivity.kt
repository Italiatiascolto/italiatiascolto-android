package it.italiatiascolto.app.ui.activities

import android.app.Activity
import android.content.Intent

class EventActivity: Activity() {


    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, EventActivity::class.java)
            activity.startActivity(intent)
        }
    }
}
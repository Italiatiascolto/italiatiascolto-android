package it.italiatiascolto.app.ui.fragments

import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import it.italiatiascolto.app.R
import it.italiatiascolto.app.databinding.FragmentEventListBinding
import it.italiatiascolto.app.recyclerview.EventRecyclerViewAdapter

class EventListFragment: BaseFragment<FragmentEventListBinding>() {
    lateinit var mEvents: ArrayList<DocumentSnapshot>
    lateinit var mEventRecyclerViewAdapter: EventRecyclerViewAdapter

    override val contentView: Int
        get() = R.layout.fragment_event_list
    
    override fun setupUi() {
        mEvents = ArrayList()
        mEventRecyclerViewAdapter = EventRecyclerViewAdapter(mEvents)
        viewDataBinding.eventsRecyclerView.adapter = mEventRecyclerViewAdapter
        viewDataBinding.eventsRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun loadContent() {
        loadEvents()
    }
    
    private fun loadEvents() {
        FirebaseFirestore.getInstance()
            .collection("events")
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->  
                mEvents = querySnapshot?.documents as ArrayList<DocumentSnapshot>
                mEventRecyclerViewAdapter.events.clear()
                mEventRecyclerViewAdapter.events.addAll(mEvents)
            }
    }
}
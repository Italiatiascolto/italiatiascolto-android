package it.italiatiascolto.app.ui.activities

import android.app.Activity
import android.content.Intent

class MainActivity: Activity() {


    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, MainActivity::class.java)
            activity.startActivity(intent)
        }
    }
}
package it.italiatiascolto.app.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.DocumentSnapshot
import it.italiatiascolto.app.R
import it.italiatiascolto.app.databinding.ItemEventBinding
import it.italiatiascolto.app.models.Event

class EventRecyclerViewAdapter(events: ArrayList<DocumentSnapshot>): RecyclerView.Adapter<EventRecyclerViewAdapter.EventRecyclerViewHolder>() {
    val events: ArrayList<DocumentSnapshot> = events


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventRecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemEventBinding>(inflater, R.layout.item_event, parent, false)
        return EventRecyclerViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return events.count()
    }

    override fun onBindViewHolder(holder: EventRecyclerViewHolder, position: Int) {
        val event = events[position]
        holder.onBind(event)
    }

    class EventRecyclerViewHolder(binding: ItemEventBinding): RecyclerView.ViewHolder(binding.root) {
        val binding: ItemEventBinding = binding

        fun onBind(event: DocumentSnapshot) {

        }

    }
}